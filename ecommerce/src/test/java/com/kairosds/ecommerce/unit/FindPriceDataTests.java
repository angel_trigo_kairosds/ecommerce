package com.kairosds.ecommerce.unit;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import com.kairosds.ecommerce.application.request.FindPriceRequest;
import com.kairosds.ecommerce.application.response.FindPriceResponse;
import com.kairosds.ecommerce.application.service.FindPriceDataUseCase;
import com.kairosds.ecommerce.domain.agregate.Brand;
import com.kairosds.ecommerce.domain.agregate.Price;
import com.kairosds.ecommerce.domain.repository.PriceRepository;
import com.kairosds.ecommerce.domain.service.PriceService;
import com.kairosds.ecommerce.domain.util.UtilFormatValue;

public class FindPriceDataTests {
	
	private final PriceRepository priceRepository = Mockito.mock(PriceRepository.class);
	private final PriceService priceService = new PriceService(priceRepository);
	private final FindPriceDataUseCase findPriceData = new FindPriceDataUseCase(priceService);
	
	@Test
	public void shouldFindDataPriceCaseOne() throws Exception {
		
		List<Price> pricesMock = getPricesTest();
		Price priceExpected = pricesMock.get(0);
		
		FindPriceResponse findPriceResponse = buildFindPriceResponse(priceExpected);
		
        LocalDateTime date = LocalDateTime.of(2020, Month.JUNE, 14, 10, 00, 00);
        
		FindPriceRequest findPriceRequestCaseOne = getFindPriceRequest(date);
		
		Mockito.when(priceRepository.findAll()).thenReturn(pricesMock);

		assertEquals(findPriceData.execute(findPriceRequestCaseOne), findPriceResponse);
		
	}
	
	@Test
	public void shouldFindDataPriceCaseTwo() throws Exception {
		
		List<Price> pricesMock = getPricesTest();
		Price priceExpected = pricesMock.get(1);
		
		FindPriceResponse findPriceResponse = buildFindPriceResponse(priceExpected);
		
        LocalDateTime date = LocalDateTime.of(2020, Month.JUNE, 14, 16, 00, 00);
        
		FindPriceRequest findPriceRequestCaseTwo = getFindPriceRequest(date);
		
		Mockito.when(priceRepository.findAll()).thenReturn(pricesMock);

		assertEquals(findPriceData.execute(findPriceRequestCaseTwo), findPriceResponse);
		
	}
	
	@Test
	public void shouldFindDataPriceCaseThree() throws Exception {
		
		List<Price> pricesMock = getPricesTest();
		Price priceExpected = pricesMock.get(0);
		
		FindPriceResponse findPriceResponse = buildFindPriceResponse(priceExpected);
		
        LocalDateTime date = LocalDateTime.of(2020, Month.JUNE, 14, 21, 00, 00);
        
		FindPriceRequest findPriceRequestCaseThree = getFindPriceRequest(date);
		
		Mockito.when(priceRepository.findAll()).thenReturn(pricesMock);

		assertEquals(findPriceData.execute(findPriceRequestCaseThree), findPriceResponse);
		
	}
	
	@Test
	public void shouldFindDataPriceCaseFour() throws Exception {
		
		List<Price> pricesMock = getPricesTest();
		Price priceExpected = pricesMock.get(2);
		
		FindPriceResponse findPriceResponse = buildFindPriceResponse(priceExpected);
		
        LocalDateTime date = LocalDateTime.of(2020, Month.JUNE, 15, 10, 00, 00);
        
		FindPriceRequest findPriceRequestCaseFour = getFindPriceRequest(date);
		
		Mockito.when(priceRepository.findAll()).thenReturn(pricesMock);

		assertEquals(findPriceData.execute(findPriceRequestCaseFour), findPriceResponse);
		
	}
	
	@Test
	public void shouldFindDataPriceCaseFive() throws Exception {
		
		List<Price> pricesMock = getPricesTest();
		Price priceExpected = pricesMock.get(3);
		
		FindPriceResponse findPriceResponse = buildFindPriceResponse(priceExpected);
		
        LocalDateTime date = LocalDateTime.of(2020, Month.JUNE, 16, 21, 00, 00);
        
		FindPriceRequest findPriceRequestCaseFive = getFindPriceRequest(date);
		
		Mockito.when(priceRepository.findAll()).thenReturn(pricesMock);

		assertEquals(findPriceData.execute(findPriceRequestCaseFive), findPriceResponse);
		
	}
	
	private FindPriceRequest getFindPriceRequest(LocalDateTime date) {
		FindPriceRequest findPriceRequest = FindPriceRequest.builder()
				.applicationDate(date)
				.productId(35455)
				.brandId(1)
				.build();
		return findPriceRequest;
	}
	
	private FindPriceResponse buildFindPriceResponse(Price priceExpected) {
		FindPriceResponse findPriceResponse = FindPriceResponse.builder()
				.productId(priceExpected.getProductId())
				.brandId(priceExpected.getBrand().getBrandId())
				.priceList(priceExpected.getPriceList())
				.startDate(UtilFormatValue.formatDateToString(priceExpected.getStartDate()))
				.endDate(UtilFormatValue.formatDateToString(priceExpected.getEndDate()))
				.finalPrice(priceExpected.getFinalPrice())
				.build();
		return findPriceResponse;
	}

	private List<Price> getPricesTest() {
		
		Brand brand = Brand.builder().brandId(1).name("brand").build();
		
		Price priceMock1 = Price.builder()
				.priceId(1l)
				.brand(brand)
				.startDate(LocalDateTime.of(2020, Month.JUNE, 14, 00, 00, 00))
				.endDate(LocalDateTime.of(2020, Month.DECEMBER, 31, 23, 59, 59))
				.priceList(1)
				.productId(35455)
				.priority(0)
				.price(35.50)
				.currency("EUR")
				.lastUpdate(LocalDateTime.of(2020, Month.MARCH, 26, 14, 49, 07))
				.lastUpdateBy("user1")
				.build();
		
		Price priceMock2 = Price.builder()
				.priceId(2l)
				.brand(brand)
				.startDate(LocalDateTime.of(2020, Month.JUNE, 14, 15, 00, 00))
				.endDate(LocalDateTime.of(2020, Month.JUNE, 14, 18, 30, 00))
				.priceList(2)
				.productId(35455)
				.priority(1)
				.price(25.45)
				.currency("EUR")
				.lastUpdate(LocalDateTime.of(2020, Month.MAY, 26, 15, 38, 22))
				.lastUpdateBy("user1")
				.build();
		
		Price priceMock3 = Price.builder()
				.priceId(3l)
				.brand(brand)
				.startDate(LocalDateTime.of(2020, Month.JUNE, 15, 00, 00, 00))
				.endDate(LocalDateTime.of(2020, Month.JUNE, 15, 11, 00, 00))
				.priceList(3)
				.productId(35455)
				.priority(1)
				.price(30.50)
				.currency("EUR")
				.lastUpdate(LocalDateTime.of(2020, Month.MAY, 26, 15, 39, 22))
				.lastUpdateBy("user2")
				.build();
		
		Price priceMock4 = Price.builder()
				.priceId(4l)
				.brand(brand)
				.startDate(LocalDateTime.of(2020, Month.JUNE, 15, 16, 00, 00))
				.endDate(LocalDateTime.of(2020, Month.DECEMBER, 31, 23, 59, 59))
				.priceList(4)
				.productId(35455)
				.priority(1)
				.price(38.95)
				.currency("EUR")
				.lastUpdate(LocalDateTime.of(2020, Month.JUNE, 2, 10, 14, 00))
				.lastUpdateBy("user1")
				.build();
		
		return List.of(priceMock1,priceMock2,priceMock3,priceMock4);
	}

}
