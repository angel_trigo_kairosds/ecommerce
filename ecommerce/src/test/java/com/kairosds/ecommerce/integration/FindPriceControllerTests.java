package com.kairosds.ecommerce.integration;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDateTime;
import java.time.Month;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;

import com.kairosds.ecommerce.application.response.FindPriceResponse;
import com.kairosds.ecommerce.domain.util.UtilFormatValue;
import com.kairosds.ecommerce.infrastructure.controller.FindPriceController;
import com.kairosds.ecommerce.infrastructure.exception.ErrorResponse;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class FindPriceControllerTests {

	@Autowired
	private FindPriceController findPriceController;

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void shouldFindResultControllerTestNotBeNull() {
		assertThat(findPriceController).isNotNull();
	}

	@Test
	public void shouldFindResultControllerTestCaseOne() {
		FindPriceResponse findPriceResponseExpected = FindPriceResponse.builder()
				.brandId(1)
				.startDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.JUNE, 14, 00, 00, 00)))
				.endDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.DECEMBER, 31, 23, 59, 59)))
				.priceList(1)
				.productId(35455)
				.finalPrice("35.50 EUR")
				.build();
		
		assertThat(restTemplate.getForObject("http://localhost:" + port + "/price" + "/find" +"/2020-06-14-10.00.00"+"/35455"+"/1", FindPriceResponse.class)).isEqualTo(findPriceResponseExpected);
	}
	
	@Test
	public void shouldFindResultControllerTestCaseTwo() {
		FindPriceResponse findPriceResponseExpected = FindPriceResponse.builder()
				.brandId(1)
				.startDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.JUNE, 14, 15, 00, 00)))
				.endDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.JUNE, 14, 18, 30, 00)))
				.priceList(2)
				.productId(35455)
				.finalPrice("25.45 EUR")
				.build();
		
		assertThat(restTemplate.getForObject("http://localhost:" + port + "/price" + "/find" +"/2020-06-14-16.00.00"+"/35455"+"/1", FindPriceResponse.class)).isEqualTo(findPriceResponseExpected);
		
	}
	
	@Test
	public void shouldFindResultControllerTestCaseTree() {
		FindPriceResponse findPriceResponseExpected = FindPriceResponse.builder()
				.brandId(1)
				.startDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.JUNE, 14, 00, 00, 00)))
				.endDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.DECEMBER, 31, 23, 59, 59)))
				.priceList(1)
				.productId(35455)
				.finalPrice("35.50 EUR")
				.build();
		
		assertThat(restTemplate.getForObject("http://localhost:" + port + "/price" + "/find" +"/2020-06-14-21.00.00"+"/35455"+"/1", FindPriceResponse.class)).isEqualTo(findPriceResponseExpected);
		
	}
	
	@Test
	public void shouldFindResultControllerTestCaseFour() {
		FindPriceResponse findPriceResponseExpected = FindPriceResponse.builder()
				.brandId(1)
				.startDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.JUNE, 15, 00, 00, 00)))
				.endDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.JUNE, 15, 11, 00, 00)))
				.priceList(3)
				.productId(35455)
				.finalPrice("30.50 EUR")
				.build();
		
		assertThat(restTemplate.getForObject("http://localhost:" + port + "/price" + "/find" +"/2020-06-15-10.00.00"+"/35455"+"/1", FindPriceResponse.class)).isEqualTo(findPriceResponseExpected);
		
	}
	
	@Test
	public void shouldFindResultControllerTestCaseFive() {
		FindPriceResponse findPriceResponseExpected = FindPriceResponse.builder()
				.brandId(1)
				.startDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.JUNE, 15, 16, 00, 00)))
				.endDate(UtilFormatValue.formatDateToString(LocalDateTime.of(2020, Month.DECEMBER, 31, 23, 59, 59)))
				.priceList(4)
				.productId(35455)
				.finalPrice("38.95 EUR")
				.build();
		
		assertThat(restTemplate.getForObject("http://localhost:" + port + "/price" + "/find" +"/2020-06-15-21.00.00"+"/35455"+"/1", FindPriceResponse.class)).isEqualTo(findPriceResponseExpected);
		
	}
	
	@Test
	public void throwResponseErrorInvalidDateFindResultControllerTest() {
		
		ErrorResponse errorReponse = restTemplate.getForObject("http://localhost:" + port + "/price" + "/find" +"/2020-06-15-error"+"/35455"+"/1", ErrorResponse.class);
		assertEquals(errorReponse.getStatus(), HttpStatus.BAD_REQUEST.value());
		assertEquals(errorReponse.getMessage(), "Invalid format date: 2020-06-15-error");
	}
	
	@Test
	public void throwResponseErrorPriceNotFoundFindResultControllerTest() {
		
		ErrorResponse errorReponse = restTemplate.getForObject("http://localhost:" + port + "/price" + "/find" +"/2020-06-15-10.00.00"+"/35455"+"/90", ErrorResponse.class);
		assertEquals(errorReponse.getStatus(), HttpStatus.BAD_REQUEST.value());
		assertEquals(errorReponse.getMessage(), "Price not found");
	}

}
