package com.kairosds.ecommerce.domain.service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

import com.kairosds.ecommerce.domain.agregate.Price;
import com.kairosds.ecommerce.domain.repository.PriceRepository;

public class PriceService {
	
	private PriceRepository priceRepository;
	
	public PriceService(PriceRepository priceRepository) {
		this.priceRepository = priceRepository;
	}

	public Price findDataPrice(LocalDateTime date, Integer productId, Integer brandId) {
		List<Price> prices = priceRepository.findAll();	
		 
		return prices.stream()
					.filter(price -> date.isAfter(price.getStartDate()))
					.filter(price -> date.isBefore(price.getEndDate()))
					.filter(price -> productId.equals(price.getProductId()))
					.filter(price -> brandId.equals(price.getBrand().getBrandId()))
					.max(Comparator.comparing(Price::getPriority)).orElseThrow(() -> new NoSuchElementException("Price not found"));
	}
}
