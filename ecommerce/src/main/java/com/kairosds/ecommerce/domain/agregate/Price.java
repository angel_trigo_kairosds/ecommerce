package com.kairosds.ecommerce.domain.agregate;

import java.time.LocalDateTime;

import com.kairosds.ecommerce.domain.util.UtilFormatValue;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Price {
	
	private Long priceId;
	
	private Brand brand;
	
	private LocalDateTime startDate;
	
	private LocalDateTime endDate;
	
	private Integer priceList;
	
	private Integer productId;
	
	private Integer priority;
	
	private Double price;
	
	private String currency;
	
	private LocalDateTime lastUpdate;
	
	private String lastUpdateBy;
	
	public String getFinalPrice () {
		return UtilFormatValue.doubleToStringTwoDecimals(price) + " " + currency;
	}


}
