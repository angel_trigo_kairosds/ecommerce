package com.kairosds.ecommerce.domain.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Locale;

public class UtilFormatValue {

	public static String formatDateToString(LocalDateTime date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss");
		try {
			return formatter.format(date);
		} catch (Exception e) {
			throw new DateTimeParseException("Invalid format date", date.toString(), 0);
		}
	}
	
	public static LocalDateTime formatStringToDate(String date) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH.mm.ss");
		try {
			return LocalDateTime.parse(date, formatter);
		} catch (Exception e) {
			throw new DateTimeParseException("Invalid format date", date, 0);
		}
		
	}
	
	public static String doubleToStringTwoDecimals(Double number) {
		NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
		DecimalFormat df = (DecimalFormat) nf;
		try {
			df.applyPattern("#.00");
			return df.format(number);
		} catch (Exception e) {
			throw new NumberFormatException("Invalid number to format:" + number);
		}
	}
	
}
