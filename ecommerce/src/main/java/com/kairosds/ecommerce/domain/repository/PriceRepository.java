package com.kairosds.ecommerce.domain.repository;

import java.util.List;

import com.kairosds.ecommerce.domain.agregate.Price;

public interface PriceRepository {

	List<Price> findAll();
}
