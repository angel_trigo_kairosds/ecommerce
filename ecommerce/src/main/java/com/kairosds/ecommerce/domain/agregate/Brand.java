package com.kairosds.ecommerce.domain.agregate;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Brand {

	private Integer brandId;
	
	private String name;
}
