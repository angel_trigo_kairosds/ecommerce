package com.kairosds.ecommerce.application.service;

import com.kairosds.ecommerce.application.request.FindPriceRequest;
import com.kairosds.ecommerce.application.response.FindPriceResponse;
import com.kairosds.ecommerce.domain.agregate.Price;
import com.kairosds.ecommerce.domain.service.PriceService;
import com.kairosds.ecommerce.domain.util.UtilFormatValue;

public class FindPriceDataUseCase {
	
	private PriceService priceService;

	public FindPriceDataUseCase(PriceService priceService) {
		this.priceService = priceService;
	}

	public FindPriceResponse execute(FindPriceRequest findPriceRequest) {
	
		Price priceFound = priceService.findDataPrice(findPriceRequest.getApplicationDate(), findPriceRequest.getProductId(), findPriceRequest.getBrandId());
		
		return FindPriceResponse.builder()
					.productId(priceFound.getProductId())
					.brandId(priceFound.getBrand().getBrandId())
					.priceList(priceFound.getPriceList())
					.startDate(UtilFormatValue.formatDateToString(priceFound.getStartDate()))
					.endDate(UtilFormatValue.formatDateToString(priceFound.getEndDate()))
					.finalPrice(priceFound.getFinalPrice())
					.build();
	}
}
