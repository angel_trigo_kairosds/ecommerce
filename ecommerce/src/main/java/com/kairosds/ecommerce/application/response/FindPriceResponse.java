package com.kairosds.ecommerce.application.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FindPriceResponse {

	private Integer productId;
	private Integer brandId;
	private Integer priceList;
	private String startDate;
	private String endDate;
	private String finalPrice;
}
