package com.kairosds.ecommerce.application.request;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FindPriceRequest {

	private LocalDateTime applicationDate;
	private Integer productId;
	private Integer brandId;
}
