package com.kairosds.ecommerce.infrastructure.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@Entity
@Table(name = "BRAND")
public class BrandEntity {
	
	@Id
	@NonNull
	@Column(name="BRAND_ID")
	private Integer brandId;

	@NonNull
	@Column(name="NAME")
	private String name;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "brand")
    private List<PriceEntity> prices;
}
