package com.kairosds.ecommerce.infrastructure.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.kairosds.ecommerce.domain.agregate.Price;
import com.kairosds.ecommerce.domain.repository.PriceRepository;
import com.kairosds.ecommerce.infrastructure.mapper.PriceMapper;
import com.kairosds.ecommerce.infrastructure.repository.PriceSpringDataRepository;

@Repository
public class PriceDao implements PriceRepository {

	@Autowired
	private PriceSpringDataRepository priceSpringDataRepository;

	@Override
	public List<Price> findAll() {
		return PriceMapper.toDomainList(priceSpringDataRepository.findAll());
	}

}
