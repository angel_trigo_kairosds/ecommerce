package com.kairosds.ecommerce.infrastructure.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.kairosds.ecommerce.application.request.FindPriceRequest;
import com.kairosds.ecommerce.application.response.FindPriceResponse;
import com.kairosds.ecommerce.application.service.FindPriceDataUseCase;
import com.kairosds.ecommerce.domain.util.UtilFormatValue;

@RestController
public class FindPriceController {

	@Autowired
	FindPriceDataUseCase findPriceData;
	
	@GetMapping("price/find/{date}/{productId}/{brandId}")
	public ResponseEntity<FindPriceResponse> findPriceData(@PathVariable String date, @PathVariable Integer productId, @PathVariable Integer brandId) throws Exception {
		return new ResponseEntity<>(findPriceData.execute(FindPriceRequest.builder().applicationDate(UtilFormatValue.formatStringToDate(date)).productId(productId).brandId(brandId).build()), HttpStatus.OK);
	}
}
