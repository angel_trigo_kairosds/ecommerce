package com.kairosds.ecommerce.infrastructure.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.kairosds.ecommerce.infrastructure.entity.PriceEntity;

@Repository
public interface PriceSpringDataRepository extends JpaRepository<PriceEntity, Long> {

}
