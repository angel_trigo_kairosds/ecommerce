package com.kairosds.ecommerce.infrastructure.exception;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorResponse {

	private int status;

	private String message;
	
	private Date timestamp;
}
