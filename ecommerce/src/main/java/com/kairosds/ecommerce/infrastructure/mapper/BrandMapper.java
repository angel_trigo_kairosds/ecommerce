package com.kairosds.ecommerce.infrastructure.mapper;

import com.kairosds.ecommerce.domain.agregate.Brand;
import com.kairosds.ecommerce.infrastructure.entity.BrandEntity;

public interface BrandMapper {
	
	public static Brand toDomain(BrandEntity brandEntity) {
		return Brand.builder().brandId(brandEntity.getBrandId()).name(brandEntity.getName()).build();
	}
}
