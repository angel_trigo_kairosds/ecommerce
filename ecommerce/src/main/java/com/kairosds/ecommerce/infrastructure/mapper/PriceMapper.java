package com.kairosds.ecommerce.infrastructure.mapper;

import java.util.List;
import java.util.stream.Collectors;

import com.kairosds.ecommerce.domain.agregate.Price;
import com.kairosds.ecommerce.infrastructure.entity.PriceEntity;

public interface PriceMapper {

	public static List<Price> toDomainList(List<PriceEntity> pricesEntity){
		return pricesEntity.stream().map(priceEntity -> toDomain(priceEntity)).collect(Collectors.toList());
	}
	
	public static Price toDomain(PriceEntity priceEntity) {
		return Price.builder()
				.priceId(priceEntity.getPriceId())
				.brand(BrandMapper.toDomain(priceEntity.getBrand()))
				.startDate(priceEntity.getStartDate())
				.endDate(priceEntity.getEndDate())
				.priceList(priceEntity.getPriceList())
				.productId(priceEntity.getProductId())
				.priority(priceEntity.getPriority())
				.price(priceEntity.getPrice())
				.currency(priceEntity.getCurrency())
				.lastUpdate(priceEntity.getLastUpdate())
				.lastUpdateBy(priceEntity.getLastUpdateBy())
				.build();
	}
}
