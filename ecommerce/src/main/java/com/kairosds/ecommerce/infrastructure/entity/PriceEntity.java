package com.kairosds.ecommerce.infrastructure.entity;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@Entity
@Table(name = "PRICE")
public class PriceEntity {
	
	@Id
	@NonNull
	@Column(name="PRICE_ID")
	private Long priceId;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "brandId")
	private BrandEntity brand;
	
	@NonNull
	@Column(name="START_DATE")
	private LocalDateTime startDate;
	
	@NonNull
	@Column(name="END_DATE")
	private LocalDateTime endDate;
	
	@NonNull
	@Column(name="PRICE_LIST")
	private Integer priceList;
	
	@NonNull
	@Column(name="PRODUCT_ID")
	private Integer productId;
	
	@NonNull
	@Column(name="PRIORITY")
	private Integer priority;
	
	@NonNull
	@Column(name="PRICE")
	private Double price;
	
	@NonNull
	@Column(name="CURR")
	private String currency;
	
	@NonNull
	@Column(name="LAST_UPDATE")
	private LocalDateTime lastUpdate;
	
	@NonNull
	@Column(name="LAST_UPDATE_BY")
	private String lastUpdateBy;
}
