package com.kairosds.ecommerce.infrastructure.exception;

import java.time.format.DateTimeParseException;
import java.util.Date;
import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice(annotations = RestController.class)
public class HandlerControllerException extends ResponseEntityExceptionHandler {

	@ExceptionHandler
	protected ResponseEntity<ErrorResponse> handleException(NoSuchElementException exc) {
		HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
		return buildResponseEntity(httpStatus, exc.getMessage());
	}
	
	@ExceptionHandler
	protected ResponseEntity<ErrorResponse> handleException(DateTimeParseException exc) {
		HttpStatus httpStatus = HttpStatus.BAD_REQUEST;
		return buildResponseEntity(httpStatus, exc.getMessage() +": "+ exc.getParsedString());
	}

	private ResponseEntity<ErrorResponse> buildResponseEntity(HttpStatus httpStatus, String message) {
		ErrorResponse error = ErrorResponse.builder().status(httpStatus.value()).message(message).timestamp(new Date())
				.build();
		return new ResponseEntity<>(error, httpStatus);
	}
	
	
}