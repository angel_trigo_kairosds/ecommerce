package com.kairosds.ecommerce;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.kairosds.ecommerce.application.service.FindPriceDataUseCase;
import com.kairosds.ecommerce.domain.repository.PriceRepository;
import com.kairosds.ecommerce.domain.service.PriceService;

@Configuration
public class SpringBootServiceConfig {

	@Bean
	public FindPriceDataUseCase findPriceData(PriceService priceService) {
		return new FindPriceDataUseCase(priceService);
	}

	@Bean
	public PriceService priceService(PriceRepository priceRepository) {
		return new PriceService(priceRepository);
	}
	
}