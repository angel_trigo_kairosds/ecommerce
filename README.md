### Summary Info
***
Project built with Hexagonal Architecture in a Spring Boot Application.

Java version 11.

This application uses a H2 Data Base, wich is a Data Base in memory.

## Installation
***
Install Lombok in your IDE to generate POJO: [LINK](https://projectlombok.org/download)

The application can be launched via docker or from the IDE that you selected.

First create jar and install dependencies:
	
	mvn clean install
	
	Update Maven project

Via Docker execute the next commands in a shell:

	cd ../ecommerce
	docker build -t ecommerce .
	docker run -p 8080:8080 ecommerce -d

## Technologies
***
A list of technologies used within the project:
* [Spring Boot](https://spring.io/blog/2022/05/19/spring-boot-2-7-0-available-now): Version 2.7.0 
* [H2 BBDD](https://www.h2database.com/html/main.html)
* [Lombok](https://projectlombok.org/setup/maven)
* [JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [JUnit](https://spring.io/guides/gs/testing-web/)
* [Docker](https://docs.docker.com/)

## How Can I Test it?
***

You have the next endpoint:

Get - http://localhost:8080/price/find/{date}/{productId}/{brandId} - Example -> http://localhost:8080/price/find/2020-06-14-10.00.00/35455/1
